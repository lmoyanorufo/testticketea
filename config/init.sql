-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 05-12-2016 a las 01:42:10
-- Versión del servidor: 5.7.16-0ubuntu0.16.04.1
-- Versión de PHP: 7.0.8-0ubuntu0.16.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ticketea`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `packs`
--

CREATE TABLE `packs` (
  `id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `promotion` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `packs`
--

INSERT INTO `packs` (`id`, `name`, `promotion`) VALUES
(1, 'Pack zumo de Naranja', 1),
(2, 'Pack menú', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pack_products`
--

CREATE TABLE `pack_products` (
  `product` int(10) NOT NULL,
  `pack` int(10) NOT NULL,
  `amount` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pack_products`
--

INSERT INTO `pack_products` (`product`, `pack`, `amount`) VALUES
(1, 2, 100),
(2, 2, 100),
(7, 2, 1),
(8, 1, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` decimal(5,2) NOT NULL,
  `amount` int(10) NOT NULL,
  `typeselling` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `amount`, `typeselling`) VALUES
(1, 'Ensalada de espinacas', '1.65', 100, 0),
(2, 'Tortellini alla carbonara', '1.30', 100, 0),
(3, 'Pollo al curry', '1.55', 100, 0),
(4, 'Arroz con verduras', '1.55', 100, 0),
(5, 'Pizza primavera', '2.00', 1, 1),
(6, 'Carne estofada', '2.15', 100, 0),
(7, 'Agua', '1.20', 1, 1),
(8, 'Zumo de naranja', '2.00', 1, 1),
(9, 'Manzana', '2.00', 1, 1),
(10, 'Tarta de queso', '2.50', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `promotions`
--

CREATE TABLE `promotions` (
  `id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `operation` varchar(2) NOT NULL,
  `amount` decimal(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `promotions`
--

INSERT INTO `promotions` (`id`, `name`, `operation`, `amount`) VALUES
(1, 'Promoción 3x2', '%', '33.00'),
(2, 'Promoción 20% ', '%', '20.00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `packs`
--
ALTER TABLE `packs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pack_products`
--
ALTER TABLE `pack_products`
  ADD PRIMARY KEY (`product`,`pack`);

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `promotions`
--
ALTER TABLE `promotions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;