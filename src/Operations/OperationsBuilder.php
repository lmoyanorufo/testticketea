<?php

namespace Operations;

use BusinessObjects\BusinessObjectsBuilder;

class OperationsBuilder
{
	private static $instance = null;
	protected $app;
    protected $mapperBuilder;

	public function __construct($app)
	{
		$this->app = $app;
        $this->mapperBuilder = \Data\MapperBuilder::getInstance($app);
	}

	public function getOperations($nameClass)
	{
        switch($nameClass){
            case 'Products':
                return new Products($this->mapperBuilder->getMapper('Products'));
            case 'Packs':      
                return new Packs(
                    $this->mapperBuilder->getMapper('Packs'),
                    BusinessObjectsBuilder::getInstance($this->app)
                );
            default:
                throw new \Exception("OPERATIONS_DOESNT_EXISTS");
        }
	}

	public static function getInstance($app)
	{
		if (null === self::$instance){
			self::$instance = new OperationsBuilder($app);
		}

		return self::$instance;
	}
}
