<?php

namespace Operations;

use Cart\CartList;
use Data\Mappers\Packs as PackMapper;
use BusinessObjects\BusinessObjectsBuilder;


class Packs
{   
    protected $packsMapper;
    protected $businessObjectsBuilder;

    public function __construct(PackMapper $packsMapper, $businessObjectsBuilder)
    {
        $this->packsMapper = $packsMapper;
        $this->businessObjectsBuilder = $businessObjectsBuilder;
    }

    public function findPacks(CartList $cart)
    {
        $listProductsIds = $cart->getListProductIds();
        
        $listPacks = [];
        foreach($listProductsIds as $productId){
            $amount = $cart->getNumItemsForProduct($productId);
            $listPacksProduct = $this->packsMapper->listProductIncludedPacks($productId, $amount);
            $listPacks = array_merge($listPacks, $listPacksProduct);
        }

        $listPacksBO = [];
        $notPackForCart = [];
        
        foreach($listPacks as $pack){
            
            $searchPack = !in_array($pack->getId(), $notPackForCart) && !in_array($pack->getId(), $listPacksBO);
            
            if (!$searchPack){
                continue;
            }
            
            $packBO = $this->businessObjectsBuilder->getBussinesObject("Pack", $pack->getId());
                
            if ($packBO->isPackForCart($cart)){
                $listPacksBO[$pack->getId()] = $packBO;
            }
            else{
                $notPackForCart[] = $pack->getId();
            }
        }
        
        return $listPacksBO;
    }
}
