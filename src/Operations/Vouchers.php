<?php

namespace Operations;


class Vouchers
{   
    protected $voucherMapper;

    public function __construct($voucherMapper)
    {
        $this->voucherMapper = $voucherMapper;
    }

    public function findVouchers()
    {
        $listVouchers = $this->voucherMapper->listAll();
        
        return $listVouchers;
    }
}
