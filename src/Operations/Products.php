<?php

namespace Operations;


class Products
{   
    const TYPE_SELLING_GRAMS = 0;
    const TYPE_SELLING_UNITIES = 1;
    

    protected $productMapper;

    public function __construct($productMapper)
    {
        $this->productMapper = $productMapper;
    }

    public function listAll()
    {
        $listProducts = $this->productMapper->listAll();
        
        return $listProducts;
    }
    
    public function read($id)
    {
        return $this->productMapper->read($id);
    }
}
