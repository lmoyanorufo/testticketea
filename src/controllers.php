<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$app['index.controller'] = function ($app) {
    return new Controllers\Products($app);
};

$app->get('/', 'index.controller:indexAction')->bind('main');
$app->post('/addToCart', 'index.controller:addToCartAction')->bind('addToCart');
$app->post('/clearCart', 'index.controller:clearCartAction')->bind('clearCart');
$app->post('/removeFromCart', 'index.controller:removeFromCartAction')->bind('removeFromCart');
$app->post('/makeRequest', 'index.controller:makeRequestCartAction')->bind('makeRequest');


$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    if ($app['debug']) {
        return;
    }

    // 404.html, or 40x.html, or 4xx.html, or error.html
    $templates = array(
        'errors/'.$code.'.html.twig',
        'errors/'.substr($code, 0, 2).'x.html.twig',
        'errors/'.substr($code, 0, 1).'xx.html.twig',
        'errors/default.html.twig',
    );

    return new Response($app['twig']->resolveTemplate($templates)->render(array('code' => $code)), $code);
});
