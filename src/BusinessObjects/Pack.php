<?php

namespace BusinessObjects;

use Cart\CartList;
use Data\MapperBuilder;
use Cart\PromotionCalculator;

class Pack
{
    protected $packProductsMapper;
    protected $promotionsMapper;
    protected $packMapper;
    protected $productsMapper;
    
    protected $packBean;
    protected $promotionBean;
    protected $packProducts;
    
    protected $pricePack;
    protected $packId;

    public function __construct($packId, MapperBuilder $mapperBuilder) 
    {
        $this->packId = $packId;
        $this->packProductsMapper = $mapperBuilder->getMapper("PackProducts");
        $this->promotionsMapper = $mapperBuilder->getMapper("Promotions");
        $this->packMapper = $mapperBuilder->getMapper("Packs");
        $this->productsMapper = $mapperBuilder->getMapper("Products");
   
        $this->packBean = $this->packMapper->read($packId);
        $this->promotionBean = $this->promotionsMapper->read($this->packBean->getPromotion());
        $this->packProducts = $this->packProductsMapper->listByPack($packId);  
        $this->pricePack = 0;
        
        $this->calculatePricePack();
    }
    
    
    public function getPricePack()
    {
        return $this->pricePack;
    }        
      
    public function isPackForCart(CartList $cart)
    {        
        return $this->numPacksForCart($cart) > 0;
    }
    
    public function numPacksForCart(CartList $cart)
    {
        $listMatchAmount = [];
        foreach($this->packProducts as $packProduct){
            
            $amountUserProduct = $cart->getNumItemsForProduct($packProduct->getProduct());
            $numPacksForProduct = 0;
            
            if ($amountUserProduct >= $packProduct->getAmount() &&  $packProduct->getAmount() > 0){
                $numPacksForProduct = $amountUserProduct / $packProduct->getAmount();
                $numPacksForProduct = floor($numPacksForProduct);
                $listMatchAmount[] = $numPacksForProduct;      
            }
            else{
                return 0;
            }  
        }
        
        return min($listMatchAmount);
    }
    
    public function getTotalDiscount(CartList $cart)
    {        
        // Numero de veces que encontramos la coincidencia en el carrito.
        $numPacksForCart = $this->numPacksForCart($cart);
        $pricePerPack = $this->getPricePack();
        
//        echo "price per pack ".$pricePerPack." Num Carts found: ".$numPacksForCart;
//        
//        echo print_r($this->packProducts);
        
        $totalPricePacksFromCart = $numPacksForCart * $pricePerPack;
        
        $promotionCalculator = new PromotionCalculator(
                $this->promotionBean, 
                $totalPricePacksFromCart
        );
        
        return $promotionCalculator->getDiscount();
    }
    
    protected function calculatePricePack()
    {
        $listProducts = $this->productsMapper->listAll();
        $productDict = [];

        foreach($listProducts as $product){
            $productDict[$product->getId()] = $product;
        }
        
        foreach($this->packProducts as $packProduct){
            
            $productBean = $productDict[$packProduct->getProduct()];
            $numItems = $packProduct->getAmount() / $productBean->getAmount();
            $this->pricePack+= $numItems * $productBean->getPrice();
        }
    }
}
            
           