<?php

namespace BusinessObjects;

class BusinessObjectsBuilder
{
	private static $instance = null;
	protected $app;
    protected $mapperBuilder;

	public function __construct($app)
	{
		$this->app = $app;
        $this->mapperBuilder = \Data\MapperBuilder::getInstance($app);
	}

	public function getBussinesObject($nameClass, $id)
	{
        switch($nameClass){
            case 'Pack':
                return new Pack($id, $this->mapperBuilder);
            default:
                throw new \Exception("OPERATIONS_DOESNT_EXISTS");
        }
	}
    
    

	public static function getInstance($app)
	{
		if (null === self::$instance){
			self::$instance = new BusinessObjectsBuilder($app);
		}

		return self::$instance;
	}
}
