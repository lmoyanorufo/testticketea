<?php

namespace Data\Beans;

class Promotion extends Bean
{
    protected $id;
    protected $name;
    protected $operation;
    protected $amount;

    public function getId()
    {
        return $this->id;
    }

    function getName()
    {
        return $this->name;
    }

    function getOperation()
    {
        return $this->operation;
    }
    
    function getAmount()
    {
        return $this->amount;
    } 
}
