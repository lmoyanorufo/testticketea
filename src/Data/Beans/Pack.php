<?php

namespace Data\Beans;

class Pack extends Bean
{
    protected $id;
    protected $name;
    protected $promotion;

    public function getId()
    {
        return $this->id;
    }

    function getName()
    {
        return $this->name;
    }

    function getPromotion()
    {
        return $this->promotion;
    }
}
