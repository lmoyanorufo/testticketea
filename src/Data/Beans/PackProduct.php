<?php

namespace Data\Beans;

class PackProduct extends Bean
{
    protected $product;
    protected $pack;
    protected $amount;
    
    
    function getProduct() 
    {
        return $this->product;
    }

    function getPack() 
    {
        return $this->pack;
    }

    function getAmount() 
    {
        return $this->amount;
    }
}
