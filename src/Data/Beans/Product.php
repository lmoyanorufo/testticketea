<?php

namespace Data\Beans;

class Product extends Bean
{
    protected $id;
    protected $name;
    protected $typeselling;
    protected $amount;
    protected $price;

    public function getId()
    {
        return $this->id;
    }

    function getName()
    {
        return $this->name;
    }

    function getTypeSelling()
    {
        return $this->typeselling;
    }

    function getAmount()
    {
        return $this->amount;
    }

    function getPrice()
    {
        return $this->price;
    }
}
