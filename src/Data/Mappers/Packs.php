<?php

namespace Data\Mappers;

use Data\Beans\Pack;


class Packs {

    protected $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function read($id)
    {
        $sql = "SELECT * from packs WHERE id = :packId";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam('packId', $id ,\PDO::PARAM_INT);
        $stmt->execute();
        
        $packRow = $stmt->fetch();
        $pack = new Pack();
        $pack->fillFromRow($packRow);
        
        return $pack;
    }
    
    public function listProductIncludedPacks($productId, $amount)
    {
        $sql = "SELECT pa.* from packs pa, pack_products pa_pr ".
               "WHERE pa_pr.pack = pa.id AND ".
               "pa_pr.product = :productId AND ".
               "pa_pr.amount <= :amount";
        
        
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam('productId', $productId ,\PDO::PARAM_INT);
        $stmt->bindParam('amount', $amount ,\PDO::PARAM_INT);
        $stmt->execute();
        
        $results = $stmt->fetchAll();

        $return = [];
        foreach ($results as $result) {

            $pack = new Pack();
            $pack->fillFromRow($result);
            $return[] = $pack;
        }
        
        return $return;
    }
}
