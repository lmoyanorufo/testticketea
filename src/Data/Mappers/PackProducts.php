<?php

namespace Data\Mappers;

use Data\Beans\PackProduct;


class PackProducts {

    protected $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function listByPack($packId)
    {
        $sql = "SELECT * from pack_products pa_pr ".
               "WHERE pack = :packId ";
        
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam('packId', $packId ,\PDO::PARAM_INT);
        $stmt->execute();
        
        $results = $stmt->fetchAll();

        $return = [];
        foreach ($results as $result) {

            $pack = new PackProduct();
            $pack->fillFromRow($result);
            $return[] = $pack;
        }
        
        return $return;
    }
}
