<?php

namespace Data\Mappers;

use Data\Beans\Promotion;

class Promotions {

    protected $db;

    public function __construct($db)
    {
        $this->db = $db;
    }
    
    public function read($id)
    {
        $sql = "SELECT * from promotions WHERE id = :promotionId";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam('promotionId', $id ,\PDO::PARAM_INT);
        $stmt->execute();
        
        $packRow = $stmt->fetch();
        $pack = new Promotion();
        $pack->fillFromRow($packRow);
        
        return $pack;
    }

    public function listAll()
    {
        $sql = "SELECT * from promotions";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $results = $stmt->fetchAll();

        $return = [];
        foreach ($results as $result) {

            $promotion = new Promotion();
            $promotion->fillFromRow($result);
            $return[] = $promotion;
        }
        
        return $return;
    }
}
