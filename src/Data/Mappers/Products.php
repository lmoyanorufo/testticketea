<?php

namespace Data\Mappers;

use Data\Beans\Product;

class Products {

    protected $db;

    public function __construct($db)
    {
        $this->db = $db;
    }
    
    public function read($id)
    {
        $sql = "SELECT * from products WHERE id = :productId";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam('productId', $id ,\PDO::PARAM_INT);
        $stmt->execute();
        
        $packRow = $stmt->fetch();
        $pack = new Product();
        $pack->fillFromRow($packRow);
        
        return $pack;
    }

    public function listAll()
    {
        $sql = "SELECT * from products";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $results = $stmt->fetchAll();

        $return = [];
        foreach ($results as $result) {

            $product = new Product();
            $product->fillFromRow($result);
            $return[] = $product;
        }
        
        return $return;
    }
}
