<?php 

namespace Data;
	
class MapperBuilder
{	
	private static $instance = null;
	protected $app;

	public function __construct($app)
	{	
		$this->app = $app;
	}

	public function getMapper($nameClass)
	{		
		$className = 'Data\\Mappers\\'.$nameClass;
		$classExists = class_exists($className);

		if (!$classExists){
			throw new \Exception("MAPPER_DOESNT_EXISTS"); 
		}

		return new $className($this->app['db']);
	}

	public static function getInstance($app)
	{
		if (null === self::$instance){
			self::$instance = new MapperBuilder($app);
		}

		return self::$instance;
	}
}