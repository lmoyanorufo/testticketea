<?php

use Silex\Application;
use Silex\Provider\AssetServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;
use Operations\Products;

$app = new Application();
$app->register(new ServiceControllerServiceProvider());
$app->register(new AssetServiceProvider());
$app->register(new TwigServiceProvider());
$app->register(new HttpFragmentServiceProvider());
$app->register(new Silex\Provider\SessionServiceProvider());
$app['twig'] = $app->extend('twig', function ($twig, $app) {

    $filterTypeProduct = new \Twig_SimpleFilter('productType', function ($product) {
            
        $amount = $product->getAmount();

        switch($product->getTypeSelling()){
            case Products::TYPE_SELLING_UNITIES:
                return $amount == 1 ?'la unidad':$amount.' unidades';

            case Products::TYPE_SELLING_GRAMS:
                return $amount == 1 ?' gramo':$amount.' gramos';
        
            default:
                return "";
        }
    });
    
    $twig->addFilter($filterTypeProduct);
    return $twig;
});

$ddbbName = getenv("TICKETEA_DDBB");
$ddbbUser = getenv("TICKETEA_DDBB_USER");
$ddbbPass = getenv("TICKETEA_DDBB_PASS");
$ddbbHost = getenv("TICKETEA_DDBB_HOST");

$app->register(new Silex\Provider\DoctrineServiceProvider(), [
    'db.options' => [
        'driver' => 'pdo_mysql',
        'host' => $ddbbHost,
        'dbname' => $ddbbName,
        'user' => $ddbbUser,
        'password' => $ddbbPass,
        'charset' => 'utf8mb4'
    ]
]);

$app->before(function ($request) {
    $request->getSession()->start();
});


return $app;
