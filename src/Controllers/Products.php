<?php


namespace Controllers;

use Symfony\Component\HttpFoundation\Request;
use Cart\CartList;
use Cart\UserCart;
use Silex\Application;

class Products
{
    protected $app;
    protected $productsOperations;
    protected $packOperations;
    protected $counterViewsOperations;
    const CURRENCY_SYMBOL = "€";

    public function __construct($app)
    {
    	$this->app = $app;

        $operationsBuilder = \Operations\OperationsBuilder::getInstance($app);
        $this->productsOperations = $operationsBuilder->getOperations("Products");
        $this->packOperations = $operationsBuilder->getOperations("Packs");
    }

    public function indexAction()
    {
        return $this->mainAction();
    }

    protected function mainAction($errorMessage = "", $successMessage = "")
    {   
        $cartProducts = $this->app['session']->get('cart',[]);
        $cartList = new CartList();
        $cartList->fillFromArray($cartProducts);

        $products = $this->productsOperations->listAll();
        $packs = $this->packOperations->findPacks($cartList);
        $userCart = new UserCart($products, $cartList, $packs);
                
        return $this->app['twig']->render('index.html.twig', [
                'products' => $products,
                'userProducts' => $userCart->getCurrentCart(),
                'totalPrice' => $userCart->getTotalPrice(),
                'totalDiscount' => $userCart->getTotalDiscount(),
                'finalPrice' => $userCart->getFinalPrice(),
                'errorMessage' => $errorMessage,
                'sucessMessage' => $successMessage,
                'currencySymbol' => self::CURRENCY_SYMBOL
            ]
        );
    }
    
    
    public function removeFromCartAction(Request $request, Application $app)
    {
        $cart = $app['session']->get('cart',[]);        
        $productId = (string)$request->get('productId', null);
                
        if (empty($productId)){
            return $app->redirect("/");        
        }

        unset($cart[$productId]);
        $app['session']->set('cart', $cart);
        
        return $app->redirect("/");        
    }
    
    public function clearCartAction(Request $request, Application $app)
    {
        $app['session']->set('cart', []);
        return $this->mainAction("Has borrado todo tu pedido :(");
    }
    
    public function makeRequestCartAction(Request $request, Application $app)
    {
        
        $cart = $app['session']->get('cart',[]);
        $cartEmpty = !count($cart);

        if ($cartEmpty){
            return $this->mainAction("Tu carrito está sin productos, elige algo y haz el pedido :D", "");
        }
        else{
            $app['session']->set('cart', []);
            return $this->mainAction("", "Pedido realizado. En breve te llegará ;)");
        }
    }
    
    public function addToCartAction(Request $request, Application $app)
    {
        $cart = $app['session']->get('cart',[]);        
        $productId = (string)$request->get('productId', null);
        
        if (empty($productId) ){
            return $app->redirect("/");        
        }
        
        $productBean = $this->productsOperations->read($productId);
        $amount = $productBean->getAmount();
        
        $inCart = isset($cart[$productId]); 
        $cart[$productId] = $inCart ? $cart[$productId] + $amount : $amount;
        $app['session']->set('cart', $cart);
        
        return $app->redirect("/");        
    }
}
