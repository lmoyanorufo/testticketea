<?php

namespace Cart;

use Cart\CartList;

class UserCart
{
    protected $listProducts;
    protected $cartList;
    protected $packs;
    protected $calculatedCart;
    protected $totalPrice;
    protected $totalDiscount = 0;
    
    /*
     * Pasar un cartList aqui.
     */
    public function __construct($listProducts, CartList $cartList, $packs)
    {
        $this->listProducts = $listProducts;
        $this->cartList = $cartList;
        $this->totalPrice = 0;
        $this->packs = $packs;
        $this->calculatedCart = [];
        $this->calculateCart();
        $this->calculateDiscounts();
    }
    
    public function getFinalPrice()
    {
        return $this->getTotalPrice() - $this->getTotalDiscount();
    }
    
    public function getTotalDiscount()
    {
        return $this->totalDiscount;
    }
    
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }
    
    public function getCurrentCart()
    {
        return $this->calculatedCart;
    }
    
    protected function calculateDiscounts()
    {
        foreach($this->packs as $pack){
            $this->totalDiscount+= $pack->getTotalDiscount($this->cartList);
        }
    }
    
    protected function calculateCart()
    {
        $dictProducts = [];
        foreach($this->listProducts as $product){
            $dictProducts[$product->getId()] = $product;
        }
        
        $listProductIds = $this->cartList->getListProductIds();
        
        foreach($listProductIds as $productId){
            
            $userAmount = $this->cartList->getNumItemsForProduct($productId);
            
            $productBean = $dictProducts[$productId];
            $price = $this->calculatePrice($productBean, $userAmount);
            $this->totalPrice += $price;
            $this->calculatedCart[] = [
                'id' => $productId,
                'name' => $productBean->getName(),
                'amount' => $userAmount,
                'price' => $price
            ];
        }
    }
    
    protected function calculatePrice($product, $userAmount)
    {
        return ($userAmount / $product->getAmount()) * $product->getPrice();        
    }
}

