<?php

namespace Cart;

use Data\Beans\Promotion;


class PromotionCalculator
{
   protected $price;
   protected $promotion;
    
   public function __construct(Promotion $promotion, $price) 
   {
       $this->promotion = $promotion;
       $this->price = $price;
   }
   
   public function getPriceAfterDiscount()
   {
       return $this->price - $this->getDiscount();
   }
   
   public function getDiscount()
   {
       switch($this->promotion->getOperation()){
           
           case '%':
               return ($this->promotion->getAmount() / 100) * $this->price;
           default:
               return 0;
       }
   }
   
   
   
   public function getListProductIds()
   {
       return array_keys($this->items);
   }
   
   public function setNumItemsForProduct($itemId, $amount)
   {
       $this->items[$itemId] = $amount;
   }
   
   public function getNumItemsForProduct($itemId)
   {    
       $definedProduct = array_key_exists($itemId, $this->items);
       return $definedProduct? $this->items[$itemId] : 0;
   }
   
   public function fillFromArray($list)
   {
       $this->items = $list;
   }
}
            
           