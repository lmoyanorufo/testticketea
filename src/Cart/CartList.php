<?php

namespace Cart;


class CartList
{
   protected $items = [];
   
   public function getListProductIds()
   {
       return array_keys($this->items);
   }
   
   public function setNumItemsForProduct($itemId, $amount)
   {
       $this->items[$itemId] = $amount;
   }
   
   public function getNumItemsForProduct($itemId)
   {    
       $definedProduct = array_key_exists($itemId, $this->items);
       return $definedProduct? $this->items[$itemId] : 0;
   }
   
   public function fillFromArray($list)
   {
       $this->items = $list;
   }
}
            
           