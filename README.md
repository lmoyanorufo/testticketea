Mum's App
==============
[![MumLogo](http://ticketea.projectsoldiers.com/img/logo.png)](http://ticketea.projectsoldiers.com)

Se trata de un ejemplo básico de un carrito de productos a los cuales
podemos aplicar varios descuentos.

Modelo de datos
---------------

El modelo de datos está compuesto por 4 tablas que definen básicamente
los productos, las promociones y los lotes de productos a los que están
vinculadas (3 zumos o un menú, etc.)

1. Products
2. Packs
3. PackProducts
4. Promotions

Por motivos de simplicidad se ha determinado que un pack solo tenga una
promoción y que las promociones sean acumulables.

Instalación
---------------

Para instalar es necesario PHP 5.5 o superior y MySQl 5.5 o superior. Para
el funcionamiento del proyecto es necesario definir las variables de la
base de datos (host,user, etc) en el VirtualHost del servidor, preferiblemente Apache. 

A continuación se muestra un ejemplo.

Ej:

```
	SetEnv TICKETEA_DDBB ticketea
	SetEnv TICKETEA_DDBB_USER ticketea
	SetEnv TICKETEA_DDBB_PASS 12345
	SetEnv TICKETEA_DDBB_HOST localhost
```


Para inicializar la base de datos, aplicar el script config/init.sql del proyecto.
También es necesario instalar las dependencias mediante composer (composer install)

Datos cargados
--------------
Se han agregado dos promociones, 3x2 en zumo de naranja y un menú compuesto por Agua, Ensalada de espinacas y Tortellini a la carbonara al 20%.

Se puede ver el ejemplo en funcionamiento en la siguiente Se puede ver el ejemplo en funcionamiento en la siguiente [link](http://ticketea.projectsoldiers.com)

