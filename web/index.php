<?php

use Symfony\Component\Debug\Debug;

$dev = true;
require_once __DIR__.'/../vendor/autoload.php';

if ($dev == true){
  // Debug::enable();
}
else{
    ini_set('display_errors', 0);
}

$app = require __DIR__.'/../src/app.php';

if ($dev == true){
    require __DIR__.'/../config/dev.php';
}
else{
    require __DIR__.'/../config/prod.php';
}

require __DIR__.'/../src/controllers.php';
$app->run();
